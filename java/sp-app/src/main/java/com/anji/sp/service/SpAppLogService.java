package com.anji.sp.service;

import com.anji.sp.model.po.SpAppLogPO;
import com.anji.sp.model.vo.SpAppLogVO;

/**
 * app  接口
 *
 * @author Kean
 * @date 2020/06/23
 */
public interface SpAppLogService {
    /**
     * 三个月复制一次表
     */
    void logTableArchive();
}