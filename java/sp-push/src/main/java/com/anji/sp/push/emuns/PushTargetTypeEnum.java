package com.anji.sp.push.emuns;

/**
 * is delete
 */
public enum PushTargetTypeEnum {
    HW("1", "华为"),
    XM("2", "小米"),
    OP("3", "Oppo"),
    VO("4", "Vivo"),
    JG_IOS("5", "极光iOS"),
    JG_ANDROID("6", "极光Android"),
    ;
    private final String code;
    private final String info;

    PushTargetTypeEnum(String code, String info) {
        this.code = code;
        this.info = info;
    }

    public String getCode() {
        return code;
    }

    public Integer getIntegerCode() {
        return Integer.parseInt(code);
    }


    public String getInfo() {
        return info;
    }
}
