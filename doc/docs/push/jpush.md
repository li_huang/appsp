# 推送-极光   

## 官网配置 
 
1、登录极光平台，点击“创建应用”，输入应用名和应用图标，如下图所示:
![avatar](../assets/jpush1.png)  
2、应用创建成功后，勾选"消息推送"服务，如下图所示:
![avatar](../assets/jpush2.png)  
3、在产品设置中，输入应用包名，如下图所示: 
![avatar](../assets/jpush3.png)  
4、下载sdk ，如下图所示: 
![avatar](../assets/jpush4.png) 

## 推送测试  

1、打开创建的应用，点击“发送消息”,如下图所示：  
![avatar](../assets/jpush6.png)   
2、添加推送消息内容，如下图所示：  
![avatar](../assets/jpush7.png)
 


