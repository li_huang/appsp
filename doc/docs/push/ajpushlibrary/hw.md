# 华为

## AppGallery Connect配置

1、	登录华为开发者平台，点击“管理中心”->“AppGallery Connect” ，进入“AppGallery Connect”中。   
2、 点击“我的项目”->“添加项目”->输入项目名称“PushTest”->“确认”，如下图所示：
![avatar](../../assets/hw1.png)  
3、	添加应用，点击“添加应用”，如下图所示：  
![avatar](../../assets/hw2.png) 
4、 在添加引用界面填写应用信息，点击“确认”，如下图所示：    
![avatar](../../assets/hw3.png)  
5、	设置SDK-添加配置文件，如下图所示，在Android Studio中配置好后点击“下一步”。  
![avatar](../../assets/hw4.png)  
6、	设置SDK-添加SDK，如下图所示，在Android Studio中配置好后点击“下一步”。
![avatar](../../assets/hw5.png)  
7、	设置SDK-集成服务，点击“完成”  
8、	设置“数据存储位置”，点击![avatar](../../assets/hw6.png)  ，选着数据存储位置，如下图所示：  
![avatar](../../assets/hw7.png)  
9、	配置“SHA256证书指纹”，以Windows系统为例：  
```
A、使用CMD命令打开命令行工具，执行cd命令进入keytool.exe所在的目录（以下样例为JDK安装在C盘的Program Files目录）
    C:\>cd C:\Program Files\Java\jdk\bin
    C:\Program Files\Java\jdk\bin>

B、执行命令keytool -list -v -keystore <keystore-file>，按命令行提示进行操作。<keystore-file>为应用签名文件的完整路径
   keytool -list -v -keystore C:\TestApp.jks

C、根据结果获取对应的SHA256指纹
```  
10、打开相关服务，点击“API管理”，打开“Push Kit”，如下图所示:  
![avatar](../../assets/hw8.png)  
11、在左侧导航栏选择“增长 > 推送服务”，点击“立即开通”，在弹出的提示框中点击“确定”  
![avatar](../../assets/hw9.png)  
12、确认开通后，您可以在“配置”页签开通和关闭您的项目级和应用级的推送服务权益  
![avatar](../../assets/hw10.png) 

## 配置AndroidManifest文件

1、配置用于接收服务器推送的消息与Token的service，该service继承HmsMessageService。ps：exported属性需要设置为false，限制其他应用的组件唤醒该service  
```
   <service
        android:name=".hw.MyPushService"
        android:exported="false">
        <intent-filter>
            <action android:name="com.huawei.push.action.MESSAGING_EVENT" />
        </intent-filter>
    </service>

```  

## 应用开发

### 申请Push Token

1、注意事项  
（1）每个设备上的每个应用的Token都是唯一存在的，客户端调用HmsInstanceId类中的getToken方法向服务端请求应用的唯一标识：Push Token.  
（2）当getToken方法返回为空时，会自动通过HmsMessageService类中的onNewToken方法获取Token值。  
（3）应用的Token要定期更新（建议应用每次启动的时候都获取Token，如果发现和上次取到的不同，需要将新获取的Token上报到自己的服务器）  
2、开发步骤
（1）调用getToken方法获取Token。
```java
    public void getToken() {
        // 创建一个新线程
        new Thread() {
            @Override
            public void run() {
                try {
                    // 从agconnect-service.json文件中读取appId
                    String appId = AGConnectServicesConfig.fromContext(context).getString("client/app_id");

                    // 输入token标识"HCM"
                    String tokenScope = "HCM";
                    String token = HmsInstanceId.getInstance(context).getToken(appId, tokenScope);
                    AppSpLog.i("get token: " + token);

                    // 判断token是否为空
                    if (!TextUtils.isEmpty(token)) {
                        AppParam.pushToken = token;
                        //将获取的pushToken上传给服务器
                        AppSpConfig.getInstance().sendRegTokenToServer(new IAppSpCallback() {
                            @Override
                            public void pushInfo(AppSpModel<String> appSpModel) {

                            }

                            @Override
                            public void error(String code, String msg) {

                            }
                        });
                    }
                } catch (ApiException e) {
                    AppSpLog.e("get token failed, " + e);
                }
            }
        }.start();
    }

```

（2）覆写onNewToken方法，Token发生变化时或者EMUI版本低于10.0以onNewToken方法返回。
```java 
    @Override
    public void onNewToken(String token) {
        // 获取token
        Log.i(TAG, "received refresh token:" + token);
    
        // 判断token是否为空
        if (!TextUtils.isEmpty(token)) {
            refreshedTokenToServer(token);
        }
    }
    private void refreshedTokenToServer(String token) {
        Log.i(TAG, "sending token to server. token:" + token);
    }
```
注意：1、调用getToken方法后获得的Token一定要做判空处理。   2、在调用getToken方法外一定要增加异常捕获处理  
（3）注销Push Token
```java
    //注销token
    public void deleteToken() {
        // 创建一个新线程
        new Thread() {
            @Override
            public void run() {
                try {
                    // 从agconnect-service.json文件中读取appId
                    String appId = AGConnectServicesConfig.fromContext(context).getString("client/app_id");

                    // 输入token标识"HCM"
                    String tokenScope = "HCM";

                    // 注销Token
                    HmsInstanceId.getInstance(context).deleteToken(appId, tokenScope);
                    AppSpLog.i("token deleted successfully");
                } catch (ApiException e) {
                    AppSpLog.e("deleteToken failed." + e);
                }
            }
        }.start();
    }
```  
### 点击通知栏跳转到指定页面

1、打开App首页  
（1）服务端发送通知消息,如下为后端传送数据格式  
```
    {
        "message": {
            "data": "{ 'score': '7', 'time': '16:42' }",
            "notification": {
                "title": "message title",
                "body": "message body"
            },
            "android": {
                "data": "{ 'androidData': '7', 'time': '16:42' }",
                "notification": {
                    "click_action": {
                        "type": 3
                    }
                }
            },
            "token": [
                "pushtoken1"
            ]
        }
    }
```  
(2)客户端“AndroidManifest.xml”文件注册主Activity
```
    <activity android:name=".MainActivity">
        <intent-filter>
            <!-- 'name'值不可改变 -->
            <action android:name="android.intent.action.MAIN"/>
            <category android:name="android.intent.category.LAUNCHER"/>
        </intent-filter>
    </activity>
```
(3)客户端App主Activity中接收数据
```java
   public class MainActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getIntentData(getIntent());
    }
 
    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        getIntentData(intent);
    }
 
    private void getIntentData(Intent intent) {
        if (null != intent) {
            // 获取的值做打点统计
            String msgid = intent.getStringExtra("_push_msgid");
            String cmdType = intent.getStringExtra("_push_cmd_type");
            int notifyId = intent.getIntExtra("_push_notifyid", -1);
 
            // 获取data里的值
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                for (String key : bundle.keySet()) {
                    String content = bundle.getString(key);
                    Log.i(TAG, "receive data from push, key = " + key + ", content = " + content);
                }
            }
            Log.i(TAG, "receive data from push, msgId = " + msgid + ", cmd = " + cmdType + ", notifyId = " + notifyId);
        } else {
            Log.i(TAG, "intent is null");
        }
    }
}
```  
2、自定义action，跳转到指定action页面  
指定action参数：指定的action参数需要与客户端“AndroidManifest.xml”文件中注册启动的Activity类中intent-filter标签中设置的action名一致  
（1）客户端“AndroidManifest.xml”文件注册待启动的Activity类  
``` 
    <activity android:name=".YourActivity">
        <intent-filter>
            <!-- ‘name’值由您自定义 -->
            <action android:name="com.huawei.codelabpush.intent.action.test" />
            <category android:name="android.intent.category.DEFAULT" />
        </intent-filter>
    </activity>
```   
（2）服务端消息体中指定action值,如下为后端传送数据格式
```
    {
        "message": {
            "data": "{ 'score': '7', 'time': '16:42' }",
            "notification": {
                "title": "message title",
                "body": "message body"
            },
            "android": {
                "data": "{ 'androidData': '7', 'time': '16:42' }",
                "notification": {
                    "click_action": {
                        "type": 1,
                        "action": "com.huawei.codelabpush.intent.action.test"
                    }
                }
            },
            "token": [
                "pushtoken1"
            ]
        }
    }
```  
（3）在自定义Activity类中接收数据，代码同打开App首页中接收数据代码相同  
注意：  
    1、支持action参数需要EMUI 10.0.0及以上，推送服务App版本为10.1.0及以上。  
    2、Push SDK 4.0及以上版本支持将action参数与intent参数通过RemoteMessage.Notification中的getClickAction()，getIntentUri()方法直接透传给应用，由应用自己处理。
    3、详细开发指导请看以下链接中的客户端开发指导[https://developer.huawei.com/consumer/cn/doc/development/HMSCore-Guides-V5/android-client-dev-0000001050042041-V5]

## 推送测试 
 
1、点击“推送服务”->“添加推送通知”，如下图所示：  
![avatar](../../assets/hw11.png)   
2、添加推送消息内容，如下图所示：  
![avatar](../../assets/hw12.png)  



